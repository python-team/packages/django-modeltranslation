django-modeltranslation (0.19.12-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Avoid django_stubs_ext in tests.
  * Use dh-sequence-python3 and dh-sequence-sphinxdoc.
  * Use pybuild-plugin-pyproject.
  * Enable autopkgtest-pkg-pybuild.
  * Build using Poetry.

 -- Colin Watson <cjwatson@debian.org>  Mon, 10 Feb 2025 11:47:48 +0000

django-modeltranslation (0.18.12-1) unstable; urgency=medium

  * Remove python3-six from build-dep and dep (Closes: #1053968)
  * New upstream version 0.18.12
  * Bump Standards-Version to 4.6.2 (no changes)

 -- Sophie Brun <sophie@freexian.com>  Mon, 16 Oct 2023 14:20:47 +0200

django-modeltranslation (0.18.11-1) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Team upload.
  * New upstream version 0.18.11.
  * Refresh d/patches.
  * Upstream migrated to pytest; adapt to it.
  * Add patch to not run converage tests.

  [ Nilesh Patra ]
  * Fix Build-Time tests to run with pytest.

 -- Utkarsh Gupta <utkarsh@debian.org>  Mon, 14 Aug 2023 22:41:00 +0530

django-modeltranslation (0.17.5-2) unstable; urgency=medium

  * Team upload.
  * Source-only upload

 -- Neil Williams <codehelp@debian.org>  Fri, 04 Feb 2022 08:18:32 +0000

django-modeltranslation (0.17.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Neil Williams <codehelp@debian.org>  Wed, 02 Feb 2022 11:23:49 +0000

django-modeltranslation (0.17.3-2) unstable; urgency=medium

  * Team upload.
  * Fix lintian missing-build-dependency-for-dh-addon error

 -- Neil Williams <codehelp@debian.org>  Fri, 08 Oct 2021 13:25:22 +0100

django-modeltranslation (0.17.3-1) unstable; urgency=medium

  * Team upload
  * [ae89df4] Adding d/gbp.conf for easier packaging
  * [46df0f6] New upstream version 0.17.3
    (Closes: #961163)
  * [1db4c29] Add a patch queue from patch queuebranch
    Added patch:
    Fix-RST-syntax-to-short-underline.patch
    Fix-changelog-import-610.patch
  * [be7c6fd] python-django-modeltranslation-doc: Add sequencer for doc-base
  * [2c394da] d/control: Add new build dependency on python3-setuptools
  * [d0342b5] python-django-modeltranslation-doc: Use dh_sphinxdoc
  * [90a2390] d/control: Update Standards-Version to 4.6.0
    No further changes needed.
  * [2697525] d/control: Adding entry Rules-Requires-Root: no
  * [de304b8] Lintian: Add override about doubled used files

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 09 Sep 2021 20:56:56 +0200

django-modeltranslation (0.16.1-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.
  * Apply multi-arch hints.
    + python-django-modeltranslation-doc: Add Multi-Arch: foreign.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Raphaël Hertzog ]
  * New upstream version 0.16.1
  * Add Build-Depends on python3-six
  * Bump Standards-Version to 4.5.1
  * Switch to debhelper compat level 13
  * Drop patches merged upstream

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 22 Jan 2021 21:34:20 +0100

django-modeltranslation (0.13.3-0.1) unstable; urgency=medium

  * Non-maintainer upload.

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support (Closes: #936416).
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0 (no changes).
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Team upload.
  * New usptream release with Django 2.2 support.

  [ Michal Arbet ]
  * Fix build with Django 2.2 (Closes: #943474).

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 14 Nov 2019 10:24:02 +0100

django-modeltranslation (0.12.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Raphaël Hertzog ]
  * New upstream release that works with current Django. Closes: #834667
  * Switch to debhelper compat level 11.
  * Bump Standards-Version to 4.1.3.
  * Update our patch to fix test failures that are due to
    django.contrib.auth.migrations being not writable. And resubmit it
    upstream.
  * Update Build-Depends to use python3-sphinx instead of python-sphinx.
  * Drop X-Python* fields.
  * Add a lintian override for privacy-breach-generic on badges embedded
    in the HTML documentation.

 -- Raphaël Hertzog <hertzog@debian.org>  Wed, 21 Feb 2018 15:19:24 +0100

django-modeltranslation (0.11-1) unstable; urgency=medium

  [ Sophie Brun ]
  * Initial release.

  [ Raphaël Hertzog ]
  * Add a patch to fix test failures due to django.contrib.auth.migrations
    being not writable.

 -- Sophie Brun <sophie@freexian.com>  Mon, 27 Jun 2016 11:20:05 +0200
